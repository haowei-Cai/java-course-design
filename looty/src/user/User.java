package user;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;  

public class User {  
	ArrayList<User> us=new ArrayList<>();
	public String uname;
	public String upassword;
	public String uid;
	public String uphone;
	int money;

	public User() {
		
	}
	
	public User(String uname,String upassword,String uid,String uphone,int money)
	{
		this.uid=uid;
		this.uname=uname;
		this.upassword=upassword;
		this.uphone=uphone;
		this.money=money;
	}
	
    
	@Override
	public String toString() {
		return "User [us=" + us + ", Uname=" + uname + ", Upassword=" + upassword + ", Uid=" + uid + ", Uphone="
				+ uphone + ", money=" + money + "]";
	}
	
	public int getMoney() {
		return money;
	}
	public void setMoney(int money) {
		this.money = money;
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		uname = uname;
	}
	public String getUpassword() {
		return upassword;
	}
	public void setUpassword(String upassword) {
		upassword = upassword;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		uid = uid;
	}
	public String getUphone() {
		return uphone;
	}
	public void setUphone(String uphone) {
		uphone = uphone;
	}
	
	
	public void show() throws IOException {
		BufferedReader in = new BufferedReader(new FileReader("C:\\1.txt"));
        String s;
        while ((s = in.readLine()) != null) {
            StringTokenizer p = new StringTokenizer(s, " ");
            String name=p.nextToken();
            String password=p.nextToken();
            String id=p.nextToken();
            String phone=p.nextToken();
            String imoney=p.nextToken();
            int money=Integer.valueOf(imoney);
            User o=new User(name,password,id,phone,money);
            us.add(o);
        }  
	}
	
	public void Registered() { 
    	
        Scanner scan=new Scanner(System.in);  
        System.out.println("请输入用户名："); 
        String iname=scan.next();
        
        System.out.println("请输入您的密码：");  
        String ipassword=scan.next();  
        
        System.out.println("请输入您的id："); 
        String id=scan.next();  
        
        System.out.println("请输入您的手机号："); 
        String phonenumber=scan.next();
        
        System.out.println("请输入充值金额：");
        String money=scan.next();
        
        File file=new File("C:\\1.txt");
    	try {
            FileWriter writer = new FileWriter(file, true);
            writer.write("\r\n"+iname+" "+ipassword+" "+id+" "+phonenumber+" "+money);
            writer.close();
       } catch (Exception ex) {
            ex.printStackTrace();
            ex.getMessage();
       }
    	
    }
    //登陆  
    public int Login() throws IOException { 
    	
        Scanner scan=new Scanner(System.in);  
        System.out.println("请输入您的用户名");  
        String iname=scan.next();  
        System.out.println("请输入您的密码：");  
        String ipassword=scan.next();  
        int flag=1;
        
        BufferedReader in = new BufferedReader(new FileReader("C:\\1.txt"));
        String s;
        while ((s = in.readLine()) != null) {
        	
            StringTokenizer p = new StringTokenizer(s, " ");
            String name=p.nextToken();
            String password=p.nextToken();
            
            if(iname.equals(name)&&ipassword.equals(password))
            {
            	System.out.println("登陆成功！");
                flag=0;  
                break;
            }
        } 
        
        if(flag==1)
        {
        	System.out.println("用户名或密码输入错误！");  
        }
        return flag;
    }

}